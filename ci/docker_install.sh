#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt-get update -yqq
apt-get install nodejs -yqq
npm install -g widdershins > /dev/null
